#!/bin/bash

# IF THE FOLLOWING VARS DO NOT EXIST, THEN THE SYSTEM ISNT PREPPED YET
if [ ! $BOOEYSAYS ] && [ ! $BOOEYSFILES ] && [ ! $BOOEYSBIN ] && [ ! $BOOEYSTEMP ] && [ ! $BOOEYSBACKUPS ] && [ ! $BOOEYSFUNCS ] & [ ! -f $RC66 ] && [ ! -f $ALIASES66 ]; then
# BASH THE DEFAULT RC FILE TO LOAD THE VARS
    . ./files/.66mhzrc;
    . ./files/.66_aliases;
# SEARCH FOR THE 66RC FILE, AND IF ITS NOT THERE,
	if [ ! -f ~/.66mhzrc ]; then
# COPY THE DEFAULT RC FILE TO ~
		cp ./files/.66mhzrc ~/;
	fi
# SEARCH FOR THE 66ALIAS FILE, AND IF ITS NOT THERE,
	if [ ! -f ~/.66_aliases ]; then
# COPY THE DEFAULT ALIASES FILE TO ~
		cp ./files/.66_aliases ~/;
	fi
# IF .BASHRC EXISTS, APPEND THE 3 LINES IN THE ADDTOBASHRC FILE
# SO THAT THE 66RC FILE WILL LOAD ON BOOT
	if [ -f ~/.bashrc ]; then
		cat ./files/addtobashrc >> ~/.bashrc;
# BUT IF IT DOES NOT EXIST, LOOK FOR .PROFILE, AND DO THE SAME FOR THAT FILE
	else
		if [ -f ~/.profile ]; then
			cat ./files/addtobashrc >> ~/.profile;
		fi
	fi
# NOW THAT ALL THE PRE-SET UP SHIT IS OUT OF THE WAY, WE MAKE THE DIRS
	mkir -p $BOOEYSAYS;
	mkir -p $BOOEYSBIN;
	mkir -p $BOOEYSFILES;
	mkir -p $BOOEYSTEMP;
	mkir -p $BOOEYSBACKUPS;
	mkir -p $BOOEYSFUNCS;


